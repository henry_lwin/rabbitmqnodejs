const amqplib = require('amqplib');

const amqp_url = "amqp://aemoadmin:S3curity.2013@localhost";
const exchange = "aemo-registration-exchange";
const participant_queue = "participant-queue";
const facility_queue = "facility-queue";
const partipant_facility_queue = "participant-facility-queue";

async function create(){
    console.log("Creating Exchange and Queues...");
    var conn = await amqplib.connect(amqp_url, "heartbeat=60");
    var ch = await conn.createChannel()
    await ch.assertExchange(exchange, 'direct', {durable: true}).catch(console.error);

    await ch.assertQueue(participant_queue, {durable: true});
    await ch.bindQueue(participant_queue, exchange, 'pq');

    await ch.assertQueue(facility_queue, {durable: true});
    await ch.bindQueue(facility_queue, exchange, 'fq');   

    await ch.assertQueue(partipant_facility_queue, {durable: true});
    await ch.bindQueue(partipant_facility_queue, exchange, 'pq');   
    await ch.bindQueue(partipant_facility_queue, exchange, 'fq');      

    setTimeout( function()  {
        console.log("Exchange(" + exchange + ")," + " and Queues(" + participant_queue + ", " 
        + facility_queue + ", " + partipant_facility_queue + ") created.");
        ch.close();
        conn.close();},  500 );
}



create();