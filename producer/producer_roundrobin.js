#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

const  queue = 'aemo-roundrobin-queue'; 
const msg = {number: process.argv[2]}
const connection_string = "amqp://aemoadmin:S3curity.2013@localhost";

amqp.connect(connection_string, function(error0, connection) {
    if (error0) {
        console.log("Error creating connection.")
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            console.log("Error creating channel.")
            throw error1;
        }

        channel.assertQueue(queue, {
            durable: true
        });
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)), {
            persistent: true
        });
        let logmsg = " [x] Sent " + msg.number + " to queue " + queue;
        console.log(logmsg);
    });
    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
});