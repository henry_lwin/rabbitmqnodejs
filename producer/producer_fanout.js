#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

const connection_string = "amqp://aemoadmin:S3curity.2013@localhost";
const exchange = 'aemo-fanout-exchange';
const msg = {number: process.argv.slice(2)}

amqp.connect(connection_string, function(error0, connection) {
    if (error0) {
        console.log("Error creating connection.");
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            console.log("Error creating channel.");
            throw error1;
        }

        channel.assertExchange(exchange, 'fanout', {
            durable: false
        });
        channel.publish(exchange, '', Buffer.from(JSON.stringify(msg)));
        let logmsg = " [x] Sent " + msg.number + " to exchange " + exchange;
        console.log(logmsg)
    });

    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
});