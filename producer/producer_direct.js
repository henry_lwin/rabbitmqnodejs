const amqplib = require('amqplib');

const amqp_url = "amqp://aemoadmin:S3curity.2013@localhost";
const exchange = "aemo-registration-exchange";

const key = process.argv[2];
const msg = process.argv.slice(3).join(' ') || 'Hello AEMO!';

async function produce(){
    console.log("Publishing");
    var conn = await amqplib.connect(amqp_url, "heartbeat=60");
    var ch = await  conn.createConfirmChannel();
    await ch.assertExchange(exchange, 'direct', {durable: true}).catch(console.error);

    await ch.publish(exchange, key, Buffer.from(msg));
    
    setTimeout( function()  {
        ch.close();
        conn.close();},  500 );
}
produce();