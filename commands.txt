// npm install amqplib

// create package.json (and add commands)
// npm init -y

// round robin (no exchange)
npm run consume_roundrobin
npm run publish_roundrobin "'roundrobin messgae-01'"

// fanout (fanout exchange, no routing key)
npm run consume_fanout
npm run publish_fanout "'fanout messgae-01'"

// direct (direct exchange, rounting key)
npm run consume_direct participant-queue
npm run consume_direct facility-queue
npm run consume_direct participant-facility-queue

npm run publish_direct pq "'participant messgae-01'"
npm run publish_direct fq "'facility messgae-01'"
