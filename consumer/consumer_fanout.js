#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

const connection_string = "amqp://aemoadmin:S3curity.2013@localhost";
const exchange = 'aemo-fanout-exchange';

amqp.connect(connection_string, function(error0, connection) {
    if (error0) {
        console.log("Error creating connection.");
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            console.log("Error creating channel.");
            throw error1;
        }

        channel.assertExchange(exchange, 'fanout', {
            durable: false
        });

        channel.assertQueue('', {
            exclusive: true
        }, function(error2, q) {
            if (error2) {
                console.log("Error creating queue.");
                throw error2;
            }
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
            channel.bindQueue(q.queue, exchange, '');

            channel.consume(q.queue, function(msg) {
                if (msg.content) {
                    let message = JSON.parse(msg.content.toString());
                    let logmsg = " [x] Received " +  message.number + " from " + q.queue;
                    console.log(logmsg);
                }
            }, {
                noAck: true
            });
        });
    });
});