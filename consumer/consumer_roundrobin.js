#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

const  queue = 'aemo-roundrobin-queue'; 
const connection_string = "amqp://aemoadmin:S3curity.2013@localhost";

amqp.connect(connection_string, function(error0, connection) {
    if (error0) {
        console.log("Error creating connection.")
        throw error0;
    }

    connection.createChannel(function(error1, channel) {
        if (error1) {
            console.log("Error creating channel.")
            throw error1;
        }
        channel.assertQueue(queue, {
            durable: true
        });
        channel.prefetch(1);
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
        channel.consume(queue, function(msg) {
            let message = JSON.parse(msg.content.toString());
            let logmsg = " [x] Received " +  message.number + " from " + queue;
            console.log(logmsg);
            setTimeout(function() {
                console.log(" [x] Done");
                channel.ack(msg);
            }, 2 * 1000);
        }, {
            noAck: false
        });
    });
});