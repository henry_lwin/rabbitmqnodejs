const amqplib = require('amqplib');

const amqp_url = "amqp://aemoadmin:S3curity.2013@localhost";
const exchange = "aemo-registration-exchange";
const participant_queue = "participant-queue";
const facility_queue = "facility-queue";
const partipant_facility_queue = "participant-facility-queue";

const queue_name = process.argv[2];

async function consume(){
    if (queue_name == null) {
        console.log("Usage: consumer_direct.js [participant-queue / facility-queue / participant-facility-queue]");
        process.exit(1);
    }
    console.log("Consuming " + queue_name + "...");
    var conn = await amqplib.connect(amqp_url, "heartbeat=60");
    var ch = await  conn.createConfirmChannel();

    await ch.assertExchange(exchange, 'direct', {durable: true}).catch(console.error);
    await ch.assertQueue(participant_queue, {durable: true});
    await ch.assertQueue(facility_queue, {durable: true});
    await ch.assertQueue(partipant_facility_queue, {durable: true});

    if (queue_name == participant_queue) {
        await ch.consume(participant_queue, function (msg) {
            console.log(msg.content.toString());
            ch.ack(msg);
        }, {consumerTag: 'myconsumer part'});                         
    } else if (queue_name == facility_queue) {
        await ch.consume(facility_queue, function (msg) {
            console.log(msg.content.toString());
            ch.ack(msg);
        }, {consumerTag: 'myconsumer fac'});            
    }     
    else if (queue_name == partipant_facility_queue) {
        await ch.consume(partipant_facility_queue, function (msg) {
            console.log(msg.content.toString());
            ch.ack(msg);
        }, {consumerTag: 'myconsumer partfac'});            
    }         

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue_name);
}


consume();
